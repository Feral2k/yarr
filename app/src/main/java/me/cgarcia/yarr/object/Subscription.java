/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.object;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Holds information related to a subscription.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Subscription implements Parcelable {

    private String id;
    private String name;
    private String link;
    private int count;
    private Bitmap icon;

    /**
     * The default constructor.
     *
     */
    public Subscription() {
        id = null;
        name = null;
        link = null;
        count = 0;
        icon = null;
    }

    /**
     * Sets up the subscription array from the
     * parcel.
     */
    public static final Creator<Subscription> CREATOR = new Creator<Subscription>() {
        @Override
        public Subscription createFromParcel(Parcel in) {
            return new Subscription(in);
        }

        @Override
        public Subscription[] newArray(int size) {
            return new Subscription[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(link);
        dest.writeInt(count);
    }

    /**
     * Assigns values from a parcel.
     *
     * @param in the parcel to read from
     */
    private Subscription(Parcel in) {
        id = in.readString();
        name = in.readString();
        link = in.readString();
        count = in.readInt();
    }

    /**
     * Returns the ID of the subscription.
     *
     * @return the ID of the subscription
     */
    public String getId() { return id; }

    /**
     * Returns the name of the subscription.
     *
     * @return the name of the subscription
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the URL of the subscription.
     *
     * @return the URL of the subscription
     */
    public String getLink() {
        return link;
    }

    /**
     * Returns the number of unread entries
     * of the subscription.
     *
     * @return the number of unread entries
     *         of the subscription
     */
    public int getCount() {
        return count;
    }

    /**
     * Returns the image associated with
     * the subscription
     *
     * @return the image associated with
     *         the subscription
     */
    public Bitmap getIcon() {
        return icon;
    }

    /**
     * Sets the ID of the subscription.
     *
     * @param id the ID of the subscription
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Sets the name of the subscription.
     *
     * @param name the name of the subscription
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the URL of the subscription.
     *
     * @param link the URL of the subscription
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * Sets the number of unread entries
     * of the subscription.
     *
     * @param count the number of unread entries
     *              of the subscription
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Sets the image associated with the
     * subscription
     *
     * @param icon the image associated with
     *             the subscription
     */
    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }
}
