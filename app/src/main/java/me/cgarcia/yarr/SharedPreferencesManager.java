/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Manages the shared preferences that are accessed
 * throughout the application.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class SharedPreferencesManager {
    // private instances of preferences
    private static SharedPreferences mPrefs;
    private static final String OPEN = "YARR";

    // strings related to key strings
    public static final String KEY_STRING_EMAIL = "email";
    public static final String KEY_STRING_PASSWORD = "password";
    public static final String KEY_STRING_DISPLAY = "display";
    public static final String KEY_STRING_TAG_ID = "tag_id";
    public static final String KEY_STRING_SUB_ID = "subscription_id";
    public static final String KEY_STRING_NOTIFICATION_URI = "notification_uri";
    public static final String KEY_STRING_SINCE = "since";

    // strings related to key integers
    public static final String KEY_INTEGER_SYNC = "sync_frequency";
    public static final String KEY_INTEGER_POSITION = "position";

    // strings related to key booleans
    public static final String KEY_BOOLEAN_LOGGED_IN = "logged_in";
    public static final String KEY_BOOLEAN_VIBRATE = "vibrate";
    public static final String KEY_BOOLEAN_NOTIFY = "notification";
    public static final String KEY_BOOLEAN_WIFI = "wifi";
    public static final String KEY_BOOLEAN_CHARGING = "charging";
    public static final String KEY_BOOLEAN_NIGHT = "night";
    public static final String KEY_BOOLEAN_FIRST_LOGIN = "first_login";

    // strings related to string values
    public static final String VALUE_STRING_DISPLAY_ALL = "all";
    public static final String VALUE_STRING_DISPLAY_STARRED = "starred";
    public static final String VALUE_STRING_DISPLAY_UNREAD = "unread";
    public static final String VALUE_STRING_DISPLAY_TAG = "tag";
    public static final String VALUE_STRING_DISPLAY_SUB = "subscription";

    // strings related to integer values
    public static final int VALUE_INTEGER_SYNC_NEVER = -1;
    public static final int VALUE_INTEGER_SYNC_15_MINUTES = 15 * 60 * 1000;
    public static final int VALUE_INTEGER_SYNC_30_MINUTES = 30 * 60 * 1000;
    public static final int VALUE_INTEGER_SYNC_1_HOUR = 60 * 60 * 1000;
    public static final int VALUE_INTEGER_SYNC_3_HOURS = 3 * 60 * 60 * 1000;
    public static final int VALUE_INTEGER_SYNC_6_HOURS = 6 * 60 * 60 * 1000;
    public static final int VALUE_INTEGER_SYNC_12_HOURS = 12 * 60 * 60 * 1000;
    public static final int VALUE_INTEGER_SYNC_1_DAY = 24 * 60 * 60 * 1000;

    // strings related to default string values
    public static final String VALUE_STRING_EMAIL_DEFAULT = "";
    public static final String VALUE_STRING_PASSWORD_DEFAULT = "";
    public static final String VALUE_STRING_DISPLAY_DEFAULT = VALUE_STRING_DISPLAY_UNREAD;
    public static final String VALUE_STRING_TAG_ID_DEFAULT = "";
    public static final String VALUE_STRING_SUB_ID_DEFAULT = "";
    public static final String VALUE_STRING_NOTIFICATION_URI_DEFAULT = null;
    public static final String VALUE_STRING_SINCE_DEFAULT = "";

    // strings related to default integer values
    public static final int VALUE_INTEGER_SYNC_DEFAULT = VALUE_INTEGER_SYNC_30_MINUTES;
    public static final int VALUE_INTEGER_POSITION_DEFAULT = 0;

    // strings related to default boolean values
    public static final boolean VALUE_BOOLEAN_LOGGED_IN_DEFAULT = false;
    public static final boolean VALUE_BOOLEAN_VIBRATE_DEFAULT = false;
    public static final boolean VALUE_BOOLEAN_NOTIFY_DEFAULT = false;
    public static final boolean VALUE_BOOLEAN_WIFI_DEFAULT = true;
    public static final boolean VALUE_BOOLEAN_CHARGING_DEFAULT = false;
    public static final boolean VALUE_BOOLEAN_NIGHT_DEFAULT = false;
    public static final boolean VALUE_BOOLEAN_FIRST_LOGIN_DEFAULT = true;

    /**
     * Initializes the global shared preferences value.
     *
     * @param context the context of the activity
     */
    static void init(Context context) {
        if (mPrefs == null) {
            // only get user preferences if not previously gotten
            mPrefs = context.getApplicationContext().getSharedPreferences(OPEN, Activity.MODE_PRIVATE);
        }
    }

    /**
     * Returns a string value from shared preferences.
     *
     * @param key      the key attribute for shared preferences
     * @param defValue the default value for shared preferences
     * @return         the string value
     */
    public static String getValue(String key, String defValue) {
        return mPrefs.getString(key, defValue);
    }

    /**
     * Returns an integer value from shared preferences.
     *
     * @param key      the key attribute for shared preferences
     * @param defValue the default value for shared preferences
     * @return         the integer value
     */
    public static int getValue(String key, int defValue) {
        return mPrefs.getInt(key, defValue);
    }

    /**
     * Returns a boolean value from shared preferences.
     *
     * @param key      the key attribute for shared preferences
     * @param defValue the default value for shared preferences
     * @return         the boolean value
     */
    public static boolean getValue(String key, boolean defValue) {
        return mPrefs.getBoolean(key, defValue);
    }

    /**
     * Sets a string value for shared preferences.
     *
     * @param key   the key attribute for shared preferences
     * @param value the default value for shared preferences
     */
    public static void setValue(String key, String value) {
        // get the shared preferences editor
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Sets a integer value for shared preferences.
     *
     * @param key   the key attribute for shared preferences
     * @param value the default value for shared preferences
     */
    public static void setValue(String key, int value) {
        // get the shared preferences editor
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * Sets a boolean value for shared preferences.
     *
     * @param key   the key attribute for shared preferences
     * @param value the default value for shared preferences
     */
    public static void setValue(String key, boolean value) {
        // get the shared preferences editor
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * Resets shared preferences to default values.
     */
    public static void clear() {
        // get the shared preferences editor
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.clear();
        editor.apply();
    }
}
