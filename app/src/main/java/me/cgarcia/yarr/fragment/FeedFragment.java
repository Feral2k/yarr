/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.FeedDatabase;
import me.cgarcia.yarr.object.Entry;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * Fragment that holds the array lists for entries,
 * subscriptions, and tags between UI changes.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class FeedFragment extends Fragment {
    // callbacks
    private TaskCallbacks mTaskCallbacks;

    // array lists
    private List<Entry> mEntries;
    private List<Subscription> mSubscriptions;
    private List<Tag> mTags;
    private List<Tag> mToggledTags;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retain this fragment
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // hold a reference to the parent activity so we can report the
        // task's current progress and results
        mTaskCallbacks = (TaskCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Set the callback to null so we don't accidentally leak the
        // activity instance.
        mTaskCallbacks = null;
    }

    /**
     * Interfaces that are executed after their
     * AsyncTask counterparts.
     */
    public interface TaskCallbacks {
        void onPostDisplayEntries();
        void onPostDisplaySubscriptions();
        void onPostDisplayTags();
    }

    /**
     * Returns the entry array list.
     *
     * @return the entry array list
     */
    public List<Entry> getEntries() {
        checkEntries();
        return mEntries;
    }

    /**
     * Returns the subscription array list.
     *
     * @return the subscription array list
     */
    public List<Subscription> getSubscriptions() {
        checkSubscriptions();
        return mSubscriptions;
    }

    /**
     * Returns the tag array list.
     *
     * @return the tag array list
     */
    public List<Tag> getTags() {
        checkTags();
        return mTags;
    }

    public List<Tag> getToggledTags() {
        checkToggledTags();
        return mToggledTags;
    }

    /**
     * Sets the entry array list.
     *
     * @param entries the entry array list
     */
    public void setEntries(List<Entry> entries) {
        mEntries = new ArrayList<>(entries);
    }

    /**
     * Sets the subscription array list.
     *
     * @param subscriptions the subscription array list
     */
    public void setSubscriptions(List<Subscription> subscriptions) {
        mSubscriptions = new ArrayList<>(subscriptions);
    }

    /**
     * Sets the tag array list.
     *
     * @param tags the tag array list
     */
    public void setTags(List<Tag> tags) {
        mTags = new ArrayList<>(tags);
    }

    /**
     * Sets the toggled tag array list.
     *
     * @param toggledTags the toggled tag array list
     */
    public void setToggledTags(List<Tag> toggledTags) {
        mToggledTags = new ArrayList<>(toggledTags);
    }

    /**
     * Calls an AsyncTask to fetch the entries from the database.
     *
     * @param query the search query
     * @param date  the date from which to get the entries
     * @param limit the number of entries to get
     */
    public void displayEntries(String query, String date, int limit) {
        AsyncTask<Void, Void, Void> displayEntriesTask = new DisplayEntriesTask(query, date, limit);
        displayEntriesTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask to fetch the subscriptions from
     * the database.
     */
    public void displaySubscriptions() {
        AsyncTask<Void, Void, Void> displaySubscriptionsTask = new DisplaySubscriptionsTask();
        displaySubscriptionsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask to fetch the tags from the database.
     */
    public void displayTags() {
        AsyncTask<Void, Void, Void> displayTagsTask = new DisplayTagsTask();
        displayTagsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask that deletes a subscription from
     * the server.
     *
     * @param subscriptionId the ID of the subscription to delete
     */
    public void deleteSubscription(String subscriptionId) {
        // call the task
        AsyncTask<Void, Void, Void> deleteSubscriptionTask =
                new DeleteSubscriptionTask(subscriptionId);
        deleteSubscriptionTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask that deletes a tag from the server.
     *
     * @param tagId the ID of the tag to delete
     */
    public void deleteTag(String tagId) {
        // call the task
        AsyncTask<Void, Void, Void> deleteTagTask = new DeleteTagTask(FeedDatabase.getTagName(tagId));
        deleteTagTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask that marks an entry from
     * the server as unread.
     *
     * @param entryId the ID of the entry to mark as unread
     */
    public void markAsUnread(String entryId) {
        AsyncTask<Void, Void, Void> markAsUnreadTask = new MarkAsUnreadTask(entryId);
        markAsUnreadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Calls an AsyncTask that marks an entry from
     * the server as read.
     *
     * @param entryId the ID of the entry to mark as read
     */
    public void markAsRead(String entryId) {
        AsyncTask<Void, Void, Void> markAsReadTask = new MarkAsReadTask(entryId);
        markAsReadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * Checks entries list to make sure that it is not null
     * and, if so, sets it to a new list.
     */
    private void checkEntries() {
        if (mEntries == null) {
            mEntries = new ArrayList<>();
        }
    }

    /**
     * Checks subscriptions list to make sure that it is not null
     * and, if so, sets it to a new list.
     */
    private void checkSubscriptions() {
        if (mSubscriptions == null) {
            mSubscriptions = new ArrayList<>();
        }
    }

    /**
     * Checks tags list to make sure that it is not null
     * and, if so, sets it to a new list.
     */
    private void checkTags() {
        if (mTags == null) {
            mTags = new ArrayList<>();
        }
    }

    /**
     * Checks toggled tags list to make sure that it is not null
     * and, if so, sets it to a new list.
     */
    private void checkToggledTags() {
        if (mToggledTags == null) {
            mToggledTags = new ArrayList<>();
        }
    }

    /**
     * AsyncTask that fetches entries from the database and
     * sets them to the entries list.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class DisplayEntriesTask extends AsyncTask<Void, Void, Void> {

        private String mSearchQuery;
        private String mDate;
        private int mLimit;

        /**
         * The default constructor.
         *
         * @param query the search query
         * @param date  the date from which to get the entries
         * @param limit the number of entries to get
         */
        DisplayEntriesTask(String query, String date, int limit) {
            mSearchQuery = query;
            mDate = date;
            mLimit = limit;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // make sure entries list is not null
            checkEntries();

            // get the current display
            String display = SharedPreferencesManager.getValue(
                    SharedPreferencesManager.KEY_STRING_DISPLAY,
                    SharedPreferencesManager.VALUE_STRING_DISPLAY_DEFAULT);

            if (mSearchQuery == null || TextUtils.isEmpty(mSearchQuery)) {
                // if not calling searching, falls back to previous display
                // as user would expect
                switch (display) {
                    case SharedPreferencesManager.VALUE_STRING_DISPLAY_ALL:
                        mEntries.addAll(FeedDatabase.getAllEntries(mDate, mLimit));
                        break;
                    case SharedPreferencesManager.VALUE_STRING_DISPLAY_STARRED:
                        mEntries.addAll(FeedDatabase.getStarredEntries(mDate, mLimit));
                        break;
                    case SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB:
                        String subId = SharedPreferencesManager.getValue(
                                SharedPreferencesManager.KEY_STRING_SUB_ID,
                                SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);
                        if (FeedDatabase.containsSubscription(subId)) {
                            // subscription still exists
                            mEntries.addAll(FeedDatabase.getEntriesBySubscription(subId, mDate, mLimit));
                        } else {
                            // subscription does not exist; go to unread
                            // entries and refresh the views
                            SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                                    SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD);
                            //updateViews(false);
                            //return;
                        }
                        break;
                    case SharedPreferencesManager.VALUE_STRING_DISPLAY_TAG:
                        String tagId = SharedPreferencesManager.getValue(
                                SharedPreferencesManager.KEY_STRING_TAG_ID,
                                SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);
                        if (FeedDatabase.containsTag(tagId)) {
                            // the tag still exists
                            mEntries.addAll(FeedDatabase.getEntriesByTag(tagId, mDate, mLimit));
                        } else {
                            // the tag does not exist; go to unread entries
                            // and refresh the views
                            SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                                    SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD);
                            //updateViews(false);
                            //return;
                        }
                        break;
                    case SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD:
                        mEntries.addAll(FeedDatabase.getUnreadEntries(mDate, mLimit));
                        break;
                }
            } else {
                // display the results of the query
                mEntries.addAll(FeedDatabase.getEntriesByQuery(mSearchQuery, mDate, mLimit));
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (mTaskCallbacks != null) {
                mTaskCallbacks.onPostDisplayEntries();
            }
        }
    }

    /**
     * AsyncTask that fetches subscriptions from the database and
     * sets them to the subscriptions list.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class DisplaySubscriptionsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            checkSubscriptions();
            mSubscriptions.clear();
            mSubscriptions.addAll(FeedDatabase.getUntaggedSubscriptions());

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (mTaskCallbacks != null) {
                mTaskCallbacks.onPostDisplaySubscriptions();
            }
        }
    }

    /**
     * AsyncTask that fetches tags from the database and
     * sets them to the tags list.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class DisplayTagsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            checkTags();
            mTags.clear();
            mTags.addAll(FeedDatabase.getAllTags());

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (mTaskCallbacks != null) {
                mTaskCallbacks.onPostDisplayTags();
            }
        }
    }

    /**
     * AsyncTask that deletes a subscription.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    private static class DeleteSubscriptionTask extends AsyncTask<Void, Void, Void> {

        String mFeedId;

        /**
         * The default constructor.
         *
         * @param feedId the ID of the subscription to delete
         */
        DeleteSubscriptionTask(String feedId) {
            mFeedId = feedId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONArray jsonSubscriptions = Feedbin.getSubscriptions();
                for (int i = 0; i < jsonSubscriptions.length(); i++) {
                    JSONObject jsonSubscription = jsonSubscriptions.getJSONObject(i);
                    if (mFeedId.equals(jsonSubscription.getString(Feedbin.JSON_FEED_ID))) {
                        String subId =
                                jsonSubscription.getString(Feedbin.JSON_ID);
                        Feedbin.deleteSubscription(subId);
                        break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * AsyncTask that deletes a tag.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    private static class DeleteTagTask extends AsyncTask<Void, Void, Void> {

        String mTagName;

        /**
         * The default constructor.
         *
         * @param tagName the name o f the tag to delete
         */
        DeleteTagTask(String tagName) {
            mTagName = tagName;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                JSONArray jsonTags = Feedbin.getTags();
                // delete all tags matching the tag name
                for (int i = 0; i < jsonTags.length(); i++) {
                    JSONObject jsonTag = jsonTags.getJSONObject(i);
                    if (jsonTag.getString(Feedbin.JSON_NAME).equals(mTagName)) {
                        String jsonTagId = jsonTag.getString(Feedbin.JSON_ID);
                        Feedbin.deleteTag(jsonTagId);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    /**
     * AsyncTask that marks a feed as unread.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    private static class MarkAsUnreadTask extends AsyncTask<Void, Void, Void> {

        private String mEntryId;

        /**
         * The default constructor.
         *
         * @param entryId the ID of the entry to mark as unread
         */
        MarkAsUnreadTask(String entryId) {
            mEntryId = entryId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // create JSON object of entry to mark as unread
            JSONObject unreadObject = new JSONObject();
            JSONArray unreadArray = new JSONArray();
            unreadArray.put(mEntryId);

            try {
                unreadObject.put(Feedbin.JSON_UNREAD_ENTRIES, unreadArray);
                // add the unread entry
                Feedbin.createUnreadEntry(unreadObject);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * AsyncTask that marks a feed as read.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    private static class MarkAsReadTask extends AsyncTask<Void, Void, Void> {

        private String mEntryId;

        /**
         * The default constructor.
         *
         * @param entryId the ID of the entry to mark as read
         */
        MarkAsReadTask(String entryId) {
            mEntryId = entryId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // crate JSON object of entry to mark as read
            JSONObject readObject = new JSONObject();
            JSONArray readArray = new JSONArray();
            readArray.put(mEntryId);

            try {
                readObject.put(Feedbin.JSON_UNREAD_ENTRIES, readArray);
                // remove the unread entry
                Feedbin.deleteUnreadEntry(readObject);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
