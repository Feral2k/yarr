/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.api.Feedbin;
import me.cgarcia.yarr.FeedDatabase;

/**
 * Fragment that renames a tag.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class RenameTagFragment extends Fragment {
    // callbacks
    private TaskCallbacks mTaskCallbacks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // retain this fragment
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // hold a reference to the parent activity so we can report the
        // task's current progress and results
        mTaskCallbacks = (TaskCallbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Set the callback to null so we don't accidentally leak the
        // activity instance.
        mTaskCallbacks = null;
    }

    /**
     * Interfaces that are executed after their
     * AsyncTask counterparts.
     */
    public interface TaskCallbacks {
        void onPostRenameTag();
    }

    /**
     * Calls an AsyncTask to rename a tag.
     *
     * @param tagId  the ID of the tag to rename
     * @param name   the new name for the tag
     */
    public void renameTag(String tagId, String name) {
        AsyncTask<Void, Void, Void> renameTagTask =
                new RenameTagTask(FeedDatabase.getTagName(tagId), name);
        renameTagTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);
    }

    /**
     * AsyncTask that attempts to modify the name of a tag.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    @SuppressLint("StaticFieldLeak")
    private class RenameTagTask extends AsyncTask<Void, Void, Void> {

        private String mOldName;
        private String mNewName;

        RenameTagTask(String oldName, String newName) {
            mOldName = oldName;
            mNewName = newName;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                // get the tags from the network
                JSONArray jsonTags = Feedbin.getTags();
                List<String> feedIds = new ArrayList<>();

                // delete the old tags and save their IDs
                for (int i = 0; i < jsonTags.length(); i++) {
                    if (jsonTags.getJSONObject(i).getString(Feedbin.JSON_NAME).equals(mOldName)) {
                        feedIds.add(jsonTags.getJSONObject(i).getString(Feedbin.JSON_FEED_ID));
                        String tagId = jsonTags.getJSONObject(i).getString(Feedbin.JSON_ID);
                        Feedbin.deleteTag(tagId);
                    }
                }

                // create new tags with the same feed ID in order
                // to connect them to the subscriptions
                for (int i = 0; i < feedIds.size(); i++) {
                    String feedId = feedIds.get(i);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(Feedbin.JSON_FEED_ID, feedId);
                    jsonObject.put(Feedbin.JSON_NAME, mNewName);
                    Feedbin.createTag(jsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mTaskCallbacks.onPostRenameTag();
        }
    }
}
