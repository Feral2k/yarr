/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.object.Entry;

/**
 * RecyclerViewAdapter that manages the views and behaviors of
 * entries in a list.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class EntryAdapter extends RecyclerView.Adapter<EntryAdapter.EntryHolder> {
    // declare the entries, inflater, and callback
    private List<Entry> mEntries;
    private LayoutInflater mInflater;
    private ItemClickCallback mItemClickCallback;

    /**
     * The default constructor.
     *
     * @param entries the list of entries to manage
     * @param c       the context of the activity
     */
    public EntryAdapter(List<Entry> entries, Context c) {
        this.mInflater = LayoutInflater.from(c);
        mEntries = entries;
    }

    @Override
    public EntryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_entry, parent, false);
        return new EntryHolder(view);
    }

    @Override
    public void onBindViewHolder(EntryHolder holder, int position) {
        // get the fields of the view
        String itemName = mEntries.get(position).getName();
        String itemDate = mEntries.get(position).getDate();
        String itemTitle = mEntries.get(position).getTitle();

        // set the name
        holder.mName.setText(itemName);

        // set the title
        if (itemTitle != null) {
            holder.mTitle.setText(DefaultApplication.fromHtml(itemTitle));
        } else {
            holder.mTitle.setText("—");
        }

        // set the typeface of the title
        if (mEntries.get(position).isRead()) {
            holder.mTitle.setTypeface(null, Typeface.NORMAL);
        } else {
            holder.mTitle.setTypeface(null, Typeface.BOLD);
        }

        // set the relative date
        holder.mDate.setText(DefaultApplication.getRelativeTimeSpanString(itemDate));
    }

    @Override
    public int getItemCount() {
        return mEntries.size();
    }

    /**
     * Interface that receives the position of an entry.
     */
    public interface ItemClickCallback {
        void onEntryItemClick(int p);
    }

    /**
     * Sets the item click callback.
     *
     * @param itemClickCallback the item click callback
     */
    public void setItemClickCallback(final ItemClickCallback itemClickCallback) {
        mItemClickCallback = itemClickCallback;
    }

    /**
     * RecyclerViewHolder that receives and holds the view
     * of the entries.
     *
     * @author  Logan Garcia
     * @version %I%, %G%
     */
    class EntryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // declare the view elements
        private TextView mName;
        private TextView mDate;
        private TextView mTitle;
        private View mContainer;

        /**
         * The default constructor.
         *
         * @param itemView the current view
         */
        EntryHolder(View itemView) {
            super(itemView);

            // get the view elements
            mName = (TextView)itemView.findViewById(R.id.list_entry_name);
            mDate = (TextView)itemView.findViewById(R.id.list_entry_date);
            mTitle = (TextView)itemView.findViewById(R.id.list_entry_title);
            mContainer = itemView.findViewById(R.id.list_entry_feed_root);

            // set the listener
            mContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.list_entry_feed_root) {
                // set the position based on the position
                // of the layout
                mItemClickCallback.onEntryItemClick(getLayoutPosition());
            }
        }
    }
}