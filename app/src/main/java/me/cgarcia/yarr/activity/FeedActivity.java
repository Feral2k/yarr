/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr.activity;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.DefaultApplication;
import me.cgarcia.yarr.EndlessRecyclerViewScrollListener;
import me.cgarcia.yarr.SharedPreferencesManager;
import me.cgarcia.yarr.UpdateService;
import me.cgarcia.yarr.DividerItemDecoration;
import me.cgarcia.yarr.R;
import me.cgarcia.yarr.adapter.EntryAdapter;
import me.cgarcia.yarr.adapter.SubscriptionAdapter;
import me.cgarcia.yarr.adapter.TagAdapter;
import me.cgarcia.yarr.FeedDatabase;
import me.cgarcia.yarr.fragment.FeedFragment;
import me.cgarcia.yarr.object.Entry;
import me.cgarcia.yarr.object.Subscription;
import me.cgarcia.yarr.object.Tag;

/**
 * Activity which is launched by default if not logged in,
 * otherwise starts the LoginActivity.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class FeedActivity extends AppCompatActivity implements EntryAdapter.ItemClickCallback,
        SubscriptionAdapter.ItemClickCallback, TagAdapter.ItemClickCallbacks,
        FeedFragment.TaskCallbacks {
    // strings related to passing arguments to intents
    private static final String KEY_STRING_QUERY = "restore_query";
    private static final String KEY_STRING_FRAGMENT = "main_fragment";
    private static final String KEY_STRING_ARRAY_LIST_ENTRY_IDS = "entry_ids";
    private static final String KEY_INTEGER_ENTRY_SIZE = "size";
    private static final String KEY_BOOLEAN_REFRESHING = "refreshing";

    // variables related to default values
    private static final String VALUE_STRING_QUERY_DEFAULT = "";
    private static final int VALUE_INTEGER_ENTRY_SIZE_DEFAULT = 25;

    // integers related to activity IDs for results
    private static final int ACTIVITY_CONTENT = 1;
    private static final int ACTIVITY_ADD_FEED = 2;
    private static final int ACTIVITY_RENAME_TAG = 3;
    private static final int ACTIVITY_EDIT_SUBSCRIPTION = 4;
    private static final int ACTIVITY_SETTINGS = 5;

    // the update receiver
    private final UpdateReceiver mUpdateReceiver = new UpdateReceiver();

    // declare the fragment
    private FeedFragment mFeedFragment;

    // lists of tags, subscriptions, and entries to display
    private List<Entry> mEntries;
    private List<Subscription> mSubscriptions;
    private List<Tag> mTags;

    // the primary drawer views
    private View mAllView;
    private View mStarredView;
    private View mUnreadView;

    // query-related declarations
    private SearchView mSearchView;
    private String mSearchQuery;
    // the list size of the entries to display
    private int mCurrentEntrySize;

    // the recycler views
    private RecyclerView mRecViewEntries;
    private RecyclerView mRecViewTags;
    private RecyclerView mRecViewSubscriptions;

    // layout items needed across the class
    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private TagAdapter mTagAdapter;
    private FloatingActionButton mFab;
    private EndlessRecyclerViewScrollListener mScrollListener;

    // the swipe refresh layouts for each view
    private SwipeRefreshLayout mRefreshEntriesLayout;
    private SwipeRefreshLayout mRefreshEmptyLayout;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean isLoggedIn = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_BOOLEAN_LOGGED_IN,
                SharedPreferencesManager.VALUE_BOOLEAN_LOGGED_IN_DEFAULT);

        // close this activity and start the login activity
        // if not logged in
        if (!isLoggedIn) {
            Intent i = new Intent(FeedActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        } else {
            //set the main layout
            setContentView(R.layout.activity_feed);

            //set the API and database
            DefaultApplication.setupApi();

            // register the receiver
            IntentFilter statusIntentFilter = new IntentFilter(UpdateService.ACTION_BROADCAST);
            LocalBroadcastManager.getInstance(this).registerReceiver(
                    mUpdateReceiver, statusIntentFilter);

            // find the fragment
            FragmentManager fm = getFragmentManager();
            mFeedFragment = (FeedFragment) fm.findFragmentByTag(KEY_STRING_FRAGMENT);

            // create the fragment and data the first time
            if (mFeedFragment == null) {
                // add the fragment
                mFeedFragment = new FeedFragment();
                fm.beginTransaction().add(mFeedFragment, KEY_STRING_FRAGMENT).commit();
            }

            // set the tags, subscriptions, and entries array
            mEntries = new ArrayList<>(mFeedFragment.getEntries());
            mTags = new ArrayList<>(mFeedFragment.getTags());
            mSubscriptions = new ArrayList<>(mFeedFragment.getSubscriptions());

            //set the toolbar
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(mToolbar);

            // set the floating action button
            mFab = (FloatingActionButton) findViewById(R.id.activity_main_fab);
            mFab.setOnClickListener(setupFabListener());

            // set the swipe refresh layouts
            mRefreshEntriesLayout =
                    (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_entries);
            mRefreshEntriesLayout.setColorSchemeResources(R.color.colorSecondary);
            mRefreshEntriesLayout.setOnRefreshListener(setupRefreshListener());

            mRefreshEmptyLayout =
                    (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_empty);
            mRefreshEmptyLayout.setColorSchemeResources(R.color.colorSecondary);
            mRefreshEmptyLayout.setOnRefreshListener(setupRefreshListener());

            // set the drawer layout
            mDrawer = (DrawerLayout) findViewById(R.id.activity_main_drawer);
            mDrawerToggle = setupDrawerToggle();
            mDrawer.addDrawerListener(mDrawerToggle);

            // set the main drawer views
            mAllView = findViewById(R.id.list_drawer_all);
            mStarredView = findViewById(R.id.list_drawer_starred);
            mUnreadView = findViewById(R.id.list_drawer_unread);

            // set the recycler view for subscriptions
            mRecViewSubscriptions = (RecyclerView) findViewById(R.id.activity_main_list_subscriptions);
            mRecViewSubscriptions.setNestedScrollingEnabled(false);
            mRecViewSubscriptions.setLayoutManager(new LinearLayoutManager(this));
            SubscriptionAdapter subscriptionAdapter = new SubscriptionAdapter(mSubscriptions, this);
            mRecViewSubscriptions.setAdapter(subscriptionAdapter);
            subscriptionAdapter.setItemClickCallback(this);

            // set the recycler view for entries
            mRecViewEntries = (RecyclerView) findViewById(R.id.activity_main_entries);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            mRecViewEntries.setLayoutManager(linearLayoutManager);
            mScrollListener = setupScrollListener(linearLayoutManager);
            mRecViewEntries.addOnScrollListener(mScrollListener);
            RecyclerView.ItemDecoration dividerItemDecoration =
                    new DividerItemDecoration(ContextCompat.getDrawable(this,
                            R.drawable.item_list_divider));
            mRecViewEntries.addItemDecoration(dividerItemDecoration);
            EntryAdapter entryAdapter = new EntryAdapter(mEntries, this);
            mRecViewEntries.setAdapter(entryAdapter);
            entryAdapter.setItemClickCallback(this);

            // set the recycler view for tags
            mRecViewTags = (RecyclerView) findViewById(R.id.activity_main_list_tags);
            mRecViewTags.setNestedScrollingEnabled(false);
            mRecViewTags.setLayoutManager(new LinearLayoutManager(this));
            restoreToggleState();

            // don't initialize search on first start of activity
            if (savedInstanceState == null) {
                // reset global variables
                mSearchQuery = VALUE_STRING_QUERY_DEFAULT;
                mCurrentEntrySize = VALUE_INTEGER_ENTRY_SIZE_DEFAULT;
            } else {
                // restore global variables
                mCurrentEntrySize = savedInstanceState.getInt(KEY_INTEGER_ENTRY_SIZE);
                mSearchQuery = savedInstanceState.getString(KEY_STRING_QUERY);
                setRefreshing(savedInstanceState.getBoolean(KEY_BOOLEAN_REFRESHING));
            }

            if (mSubscriptions.size() > 0) {
                updateLists(true);
            } else {
                updateLists(false);
            }

            // determine whether user is logging in for the first time
            boolean firstLogin = SharedPreferencesManager.getValue(
                    SharedPreferencesManager.KEY_BOOLEAN_FIRST_LOGIN,
                    SharedPreferencesManager.VALUE_BOOLEAN_FIRST_LOGIN_DEFAULT);

            if (firstLogin) {
                // set progress indicator and update
                setView();
                setRefreshing(true);
                updateRemote();
            }

            // set on click listeners for main drawer views
            mAllView.setOnClickListener(setupActionListener(
                    SharedPreferencesManager.VALUE_STRING_DISPLAY_ALL));

            mStarredView.setOnClickListener(setupActionListener(
                    SharedPreferencesManager.VALUE_STRING_DISPLAY_STARRED));

            mUnreadView.setOnClickListener(setupActionListener(
                    SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD));

            // handle any intents
            handleIntent(getIntent());
        }
    }

    @Override
    protected void onDestroy() {
        // unregister the receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mUpdateReceiver);

        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // store the large arrays in the fragment
        mFeedFragment.setEntries(mEntries);
        mFeedFragment.setSubscriptions(mSubscriptions);
        mFeedFragment.setTags(mTags);
        saveToggleState();

        // save smaller variables in the bundle
        outState.putInt(KEY_INTEGER_ENTRY_SIZE, mCurrentEntrySize);
        outState.putBoolean(KEY_BOOLEAN_REFRESHING, isRefreshing());

        // save the current user query
        if (mSearchView != null) {
            mSearchQuery = mSearchView.getQuery().toString();
            outState.putString(KEY_STRING_QUERY, mSearchQuery);
        } else {
            outState.putString(KEY_STRING_QUERY, VALUE_STRING_QUERY_DEFAULT);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTIVITY_CONTENT:
                if (resultCode == RESULT_OK) {
                    // update all views after viewing entry content
                    updateLists(false);
                }
                break;
            case ACTIVITY_ADD_FEED:
                if (resultCode == RESULT_OK) {
                    // update from network after adding a feed
                    updateRemote();
                }
                break;
            case ACTIVITY_RENAME_TAG:
                if (resultCode == RESULT_OK) {
                    // update all views after renaming a tag
                    updateLists(false);
                }
                break;
            case ACTIVITY_EDIT_SUBSCRIPTION:
                if (resultCode == RESULT_OK) {
                    // update all views after editing a subscription
                    updateLists(false);
                }
                break;
            case ACTIVITY_SETTINGS:
                if (resultCode == RESULT_OK) {
                    // recreate the activity after applying
                    // new settings and preferences
                    recreate();
                }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // sync the toggle state after onRestoreInstanceState has occurred
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        // close the drawer on back pressed if it is open
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.menu_feed, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        // set the search view properties
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        if (searchManager != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }

        mSearchView.setMaxWidth(Integer.MAX_VALUE);

        // set the expand/collapse listener for the search view
        MenuItemCompat.OnActionExpandListener expandListener = setupSearchListener();
        MenuItemCompat.setOnActionExpandListener(searchItem, expandListener);

        if (mSearchQuery != null && !TextUtils.isEmpty(mSearchQuery)) {
            // set the search query if searching
            searchItem.expandActionView();
            mSearchView.setQuery(mSearchQuery, true);
            searchItem.getActionView().clearFocus();
        }

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // set the menu items that will change depending on the view
        MenuItem editItem = menu.findItem(R.id.action_edit);
        MenuItem renameItem = menu.findItem(R.id.action_rename);
        MenuItem deleteTagItem = menu.findItem(R.id.action_tag_delete);
        MenuItem deleteSubItem = menu.findItem(R.id.action_sub_delete);
        MenuItem markReadItem = menu.findItem(R.id.action_mark_read);
        MenuItem markUnreadItem = menu.findItem(R.id.action_mark_unread);

        // get the current display view
        String display = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_DISPLAY,
                SharedPreferencesManager.VALUE_STRING_DISPLAY_DEFAULT);

        switch (display) {
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_ALL:
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_STARRED:
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD:
                // common elements whether view is all, starred, or unread
                editItem.setVisible(false);
                renameItem.setVisible(false);
                deleteTagItem.setVisible(false);
                deleteSubItem.setVisible(false);
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_TAG:
                // set items relevant to a tag view
                editItem.setVisible(false);
                renameItem.setVisible(true);
                deleteTagItem.setVisible(true);
                deleteSubItem.setVisible(false);
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB:
                // set items relevant to a subscription view
                editItem.setVisible(true);
                renameItem.setVisible(false);
                deleteTagItem.setVisible(false);
                deleteSubItem.setVisible(true);
                break;
        }

        List<String> entryIds = new ArrayList<>(getEntryIds());
        List<String> unreadEntryIds = new ArrayList<>(FeedDatabase.getUnreadEntryIds());

        if (entryIds.size() == 0) {
            // no items to mark as read/unread
            markReadItem.setVisible(false);
            markUnreadItem.setVisible(false);
        } else {
            entryIds.retainAll(unreadEntryIds);

            if (entryIds.size() > 0) {
                // items to mark as read
                markReadItem.setVisible(true);
                markUnreadItem.setVisible(false);
            } else {
                // only items to mark as unread
                markReadItem.setVisible(false);
                markUnreadItem.setVisible(true);
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                break;
            case R.id.action_tag_delete:
                deleteTag();
                break;
            case R.id.action_sub_delete:
                deleteSubscription();
                break;
            case R.id.action_mark_read:
                markAllAsRead();
                break;
            case R.id.action_mark_unread:
                markAllAsUnread();
                break;
            case R.id.action_rename:
                // start rename tag intent
                intent = new Intent(FeedActivity.this, RenameTagActivity.class);
                startActivityForResult(intent, ACTIVITY_RENAME_TAG);
                break;
            case R.id.action_edit:
                // start edit subscription intent
                intent = new Intent(FeedActivity.this, EditSubscriptionActivity.class);
                startActivityForResult(intent, ACTIVITY_EDIT_SUBSCRIPTION);
                break;
            case R.id.action_refresh:
                // update everything if not already updating
                setRefreshing(true);
                updateRemote();
                break;
            case R.id.action_settings:
                // start settings intent
                intent = new Intent(FeedActivity.this, SettingsActivity.class);
                startActivityForResult(intent, ACTIVITY_SETTINGS);
                break;
        }

        return true;
    }

    @Override
    public void onPostDisplayEntries() {
        // clear the current entries
        mEntries.clear();

        // set the new entries
        mEntries.addAll(mFeedFragment.getEntries());

        // refresh the adapter
        mRecViewEntries.getAdapter().notifyDataSetChanged();

        // don't reset the listener if there are no
        // more entries to load
        if (mEntries.size() >= mCurrentEntrySize) {
            mScrollListener.resetState();
        }

        // refresh the view
        setView();
    }

    @Override
    public void onPostDisplaySubscriptions() {
        // clear the current subscriptions
        mSubscriptions.clear();

        // set the new subscriptions
        mSubscriptions.addAll(mFeedFragment.getSubscriptions());

        // refresh the adapter
        mRecViewSubscriptions.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onPostDisplayTags() {
        mTags.clear();
        mTags.addAll(mFeedFragment.getTags());
        saveToggleState();
        restoreToggleState();
    }

    @Override
    public void onEntryItemClick(int p) {
        // set the position of the entry item clicked
        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_INTEGER_POSITION, p);
        ArrayList<String> entries = new ArrayList<>(getEntryIds());

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(KEY_STRING_ARRAY_LIST_ENTRY_IDS, entries);

        // pass the entry IDs to the new activity
        Intent i = new Intent(FeedActivity.this, ContentActivity.class);
        i.putExtras(bundle);
        startActivityForResult(i, ACTIVITY_CONTENT);
    }

    @Override
    public void onUntaggedSubscriptionItemClick(int p) {
        // get the subscription at the position clicked
        Subscription subscription = mSubscriptions.get(p);

        // set the view and current subscription ID to display the subscription
        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID,
                subscription.getId());
        SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB);

        // mark item as selected and close the drawer
        onDrawerItemSelected();
    }

    @Override
    public void onTaggedSubscriptionItemClick(int p) {
        // set values to find the subscription
        int calcPosition = 0;
        boolean found = false;

        // find the subscription ID based on its relative position
        for (int i = 0; i < mTags.size() && !found; i++) {
            if (mTagAdapter.isGroupExpanded(mTags.get(i))) {
                List<Subscription> subs = mTags.get(i).getItems();
                for (int v = 0; v < subs.size(); v++) {
                    calcPosition++;
                    Subscription sub = subs.get(v);
                    if (calcPosition == p) {
                        // save the subscription ID and set the view
                        SharedPreferencesManager.setValue(
                                SharedPreferencesManager.KEY_STRING_SUB_ID, sub.getId());
                        SharedPreferencesManager.setValue(
                                SharedPreferencesManager.KEY_STRING_DISPLAY,
                                SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB);

                        // exit out of loop if found
                        found = true;
                        break;
                    }
                }
            }

            calcPosition++;
        }

        // select the subscription and close the drawer
        onDrawerItemSelected();
    }

    @Override
    public void onTagItemClick(int p) {
        // set values to find the tag
        int calcPosition = 0;

        for (int i = 0; i < mTags.size(); i++) {
            if (calcPosition == p) {
                // save the subscription ID and set the view
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID,
                        mTags.get(i).getId());
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                        SharedPreferencesManager.VALUE_STRING_DISPLAY_TAG);
                break;
            }
            else if (mTagAdapter.isGroupExpanded(mTags.get(i))) {
                calcPosition += mTags.get(i).getItems().size();
            }

            calcPosition++;
        }

        onDrawerItemSelected();
    }

    /**
     * Returns the drawer toggle that indicates whether the drawer is
     * being opened or closed.
     *
     * @return the drawer toggle
     */
    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid mToolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.action_drawer_open,
                R.string.action_drawer_close);
    }

    /**
     * Returns the action expand listener which listens when the search
     * field is expanded or collapsed.
     *
     * @return the menu item action expand listener
     */
    private MenuItemCompat.OnActionExpandListener setupSearchListener() {
        return new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // disable the refresh, drawer, and fab
                mRefreshEntriesLayout.setEnabled(false);
                mRefreshEmptyLayout.setEnabled(false);
                mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                mFab.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // enable the refresh, drawer, and fab
                mRefreshEntriesLayout.setEnabled(true);
                mRefreshEmptyLayout.setEnabled(true);
                mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                mFab.setVisibility(View.VISIBLE);
                // disable the search
                mSearchQuery = VALUE_STRING_QUERY_DEFAULT;
                invalidateOptionsMenu();
                displayEntries(false);
                return true;
            }
        };
    }

    /**
     * Returns the on click listener used to listen to click events
     * related to the three main drawer buttons (unread, starred, and
     * all).
     *
     * @param display the selected display item
     * @return        the listener for the display item
     */
    private View.OnClickListener setupActionListener(final String display) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // set display and close the drawer
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_DISPLAY,
                        display);
                onDrawerItemSelected();
            }
        };
    }

    /**
     * Returns the on refresh listener used to create the pull-down
     * refresh icon and start related update.
     *
     * @return the on refresh listener
     */
    private SwipeRefreshLayout.OnRefreshListener setupRefreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateRemote();
            }
        };
    }

    /**
     * Returns the on click listener that listens to click events
     * related to the floating action button (fab).
     *
     * @return the on click listener
     */
    private View.OnClickListener setupFabListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // start the add new subscription intent on click
                Intent i = new Intent(FeedActivity.this, AddSubscriptionActivity.class);
                startActivityForResult(i, ACTIVITY_ADD_FEED);
            }
        };
    }

    /**
     * Returns the scroll listener that triggers when a
     * certain visible threshold is met.
     *
     * @return the scroll listener
     */
    private EndlessRecyclerViewScrollListener setupScrollListener(LinearLayoutManager linearLayoutManager) {
        return new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // triggered only when new data needs to be appended to the list
                displayEntries(true);
            }
        };
    }

    /**
     * Returns the current refresh layout view that is visible on screen.
     *
     * @return the current refresh layout
     */
    private View getView() {
        if (mRefreshEntriesLayout.getVisibility() == View.VISIBLE) {
            return mRefreshEntriesLayout;
        } else {
            return mRefreshEmptyLayout;
        }
    }

    private List<String> getEntryIds() {
        String display = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_DISPLAY,
                SharedPreferencesManager.VALUE_STRING_DISPLAY_DEFAULT);

        if (mSearchQuery == null || mSearchQuery.isEmpty()) {
            switch (display) {
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_ALL:
                    return FeedDatabase.getAllEntryIds();
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_STARRED:
                    return FeedDatabase.getStarredEntryIds();
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD:
                    return FeedDatabase.getUnreadEntryIds();
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB:
                    String subscriptionId = SharedPreferencesManager.getValue(
                            SharedPreferencesManager.KEY_STRING_SUB_ID,
                            SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);
                    return FeedDatabase.getEntryIdsBySubscription(subscriptionId);
                case SharedPreferencesManager.VALUE_STRING_DISPLAY_TAG:
                    String tagId = SharedPreferencesManager.getValue(
                            SharedPreferencesManager.KEY_STRING_TAG_ID,
                            SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);
                    return FeedDatabase.getEntryIdsByTag(tagId);
            }
        }

        return FeedDatabase.getEntryIdsByQuery(mSearchQuery);
    }

    /**
     * Displays a message if there is no network connection.
     *
     * @return the boolean value of the connection
     */
    private boolean isConnected() {
        if (DefaultApplication.isConnected(this)) {
            // does not display the message
            return true;
        }

        // defaults to displaying the message
        Snackbar.make(getView(), R.string.error_no_network,
                Snackbar.LENGTH_LONG).show();

        return false;
    }

    /**
     * Returns the boolean value of whether the entries layout or the
     * empty layout is currently displaying the refreshing icon.
     *
     * @return the boolean value of a refresh layout
     */
    private boolean isRefreshing() {
        return mRefreshEntriesLayout.isRefreshing() || mRefreshEmptyLayout.isRefreshing();
    }

    /**
     * Sets the refresh layout view to display based on the amount of
     * entries that are visible.
     */
    private void setView() {
        // check if there are entries and if the view is gone
        if (mEntries.size() > 0) {
            mRefreshEntriesLayout.setVisibility(View.VISIBLE);
            mRefreshEmptyLayout.setVisibility(View.GONE);
        } else if (mEntries.size() == 0) {
            mRefreshEntriesLayout.setVisibility(View.GONE);
            mRefreshEmptyLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Activates or deactivates the refresh icon based on which layout
     * is currently visible.
     *
     * @param refreshing show/hide the refresh icon
     */
    private void setRefreshing(boolean refreshing) {
        if (mRefreshEntriesLayout.getVisibility() == View.VISIBLE) {
            // set the refresh entries layout
            mRefreshEntriesLayout.setRefreshing(refreshing);
        } else if (mRefreshEmptyLayout.getVisibility() == View.VISIBLE) {
            // set the refresh empty layout
            mRefreshEmptyLayout.setRefreshing(refreshing);
        }
    }

    /**
     * Processes an external event from an intent.
     *
     * @param intent the intent to process
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // get the search query and process it
            mSearchQuery = intent.getStringExtra(SearchManager.QUERY);
            mCurrentEntrySize = VALUE_INTEGER_ENTRY_SIZE_DEFAULT;
            displayEntries(false);
        }
    }

    /**
     * Triggers a remote update to update the database
     * information from the server.
     */
    private void updateRemote() {
        Intent intent = new Intent(getApplicationContext(), UpdateService.class);
        this.startService(intent);
    }

    /**
     * Highlights the currently selected drawer item and refreshes
     * the tag and subscriptions list before closing the drawer.
     */
    private void onDrawerItemSelected() {
        // highlight the currently selected item
        setSelection();

        // reset the current entry size
        mCurrentEntrySize = VALUE_INTEGER_ENTRY_SIZE_DEFAULT;

        // update the subscription and tag lists
        mRecViewSubscriptions.getAdapter().notifyDataSetChanged();
        saveToggleState();
        restoreToggleState();

        // close the drawer
        mDrawer.closeDrawers();

        // reset scroll position and display the corresponding entries
        mRecViewEntries.scrollToPosition(0);
        displayEntries(false);
    }

    /**
     * Highlights the currently selected item and deselects
     * all other items.
     */
    public void setSelection() {
        // get the current display
        String display = SharedPreferencesManager.getValue(
                SharedPreferencesManager.KEY_STRING_DISPLAY,
                SharedPreferencesManager.VALUE_STRING_DISPLAY_DEFAULT);

        // select display and set others to false
        switch (display) {
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_ALL:
                setTitle(R.string.dialogue_all);

                mAllView.setSelected(true);
                mUnreadView.setSelected(false);
                mStarredView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID, "");
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID, "");
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_STARRED:
                setTitle(R.string.dialogue_starred);

                mStarredView.setSelected(true);
                mUnreadView.setSelected(false);
                mAllView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID, "");
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID, "");
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD:
                setTitle(R.string.dialogue_unread);

                mUnreadView.setSelected(true);
                mAllView.setSelected(false);
                mStarredView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID, "");
                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID, "");
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_TAG:
                String tagId = SharedPreferencesManager.getValue(
                        SharedPreferencesManager.KEY_STRING_TAG_ID,
                        SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);
                setTitle(FeedDatabase.getTagName(tagId));

                mUnreadView.setSelected(false);
                mAllView.setSelected(false);
                mStarredView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_SUB_ID, "");
                break;
            case SharedPreferencesManager.VALUE_STRING_DISPLAY_SUB:
                String subId = SharedPreferencesManager.getValue(
                        SharedPreferencesManager.KEY_STRING_SUB_ID,
                        SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);
                setTitle(FeedDatabase.getSubscriptionName(subId));

                mUnreadView.setSelected(false);
                mAllView.setSelected(false);
                mStarredView.setSelected(false);

                SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_STRING_TAG_ID, "");
                break;
        }
    }

    /**
     * Saves the current expand/collapse state
     * of all tags in the drawer.
     */
    private void saveToggleState() {
        // store the toggle groups
        ArrayList<Tag> toggledGroups = new ArrayList<>();

        try {
            for (int i = 0; i < mTags.size(); i++) {
                if (mTagAdapter.isGroupExpanded(mTags.get(i))) {
                    toggledGroups.add(mTags.get(i));
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        mFeedFragment.setToggledTags(toggledGroups);
    }

    /**
     * Restores the current expand/collapse state
     * of all tags in the drawer.
     */
    private void restoreToggleState() {
        List<Tag> toggledGroups = new ArrayList<>(mFeedFragment.getToggledTags());
        mTagAdapter = new TagAdapter(mTags, this);

        // toggle the stored groups
        for (int i = toggledGroups.size() - 1; i >= 0; i--) {
            if (mTagAdapter.getGroups().contains(toggledGroups.get(i))) {
                mTagAdapter.toggleGroup(toggledGroups.get(i));
            }
        }

        // set the new adapter for the toggled groups
        mRecViewTags.setAdapter(mTagAdapter);
        mTagAdapter.setItemClickCallbacks(this);
    }

    /**
     * Displays a confirmation dialogue window and, on confirmation,
     * deletes the currently displayed tag.
     */
    private void deleteTag() {
        // set the confirmation dialogue box
        new AlertDialog.Builder(this)
                .setTitle(R.string.popup_delete_tag_title)
                .setMessage(R.string.popup_delete_tag_message)
                .setPositiveButton(R.string.popup_delete_positive,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (isConnected()) {
                                    String tagId = SharedPreferencesManager.getValue(
                                            SharedPreferencesManager.KEY_STRING_TAG_ID,
                                            SharedPreferencesManager.VALUE_STRING_TAG_ID_DEFAULT);

                                    // delete the tag from the network and local database
                                    FeedDatabase.deleteTag(tagId);
                                    mFeedFragment.deleteTag(tagId);

                                    // refresh the display
                                    SharedPreferencesManager.setValue(
                                            SharedPreferencesManager.KEY_STRING_DISPLAY,
                                            SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD);
                                    updateLists(false);
                                }
                            }
                        })
                .setNegativeButton(R.string.popup_delete_negative, null)
                .show();
    }

    /**
     * Displays a confirmation dialogue window and, on confirmation,
     * deletes the currently displayed subscription.
     */
    private void deleteSubscription() {
        // set the confirmation dialogue box
        new AlertDialog.Builder(this)
                .setTitle(R.string.popup_delete_subscription_title)
                .setMessage(R.string.popup_delete_subscription_message)
                .setPositiveButton(R.string.popup_delete_positive,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (isConnected()) {
                                    // get the subscription ID from stored value
                                    String subId = SharedPreferencesManager.getValue(
                                            SharedPreferencesManager.KEY_STRING_SUB_ID,
                                            SharedPreferencesManager.VALUE_STRING_SUB_ID_DEFAULT);

                                    // delete the subscription from the network and local database
                                    FeedDatabase.deleteSubscription(subId);
                                    mFeedFragment.deleteSubscription(subId);

                                    // refresh the display
                                    SharedPreferencesManager.setValue(
                                            SharedPreferencesManager.KEY_STRING_DISPLAY,
                                            SharedPreferencesManager.VALUE_STRING_DISPLAY_UNREAD);
                                    updateLists(false);
                                }
                            }
                        })
                .setNegativeButton(R.string.popup_delete_negative, null)
                .show();
    }

    /**
     * Marks the currently displayed entries on screen as read and
     * provides an undo option to reverse the action.
     */
    private void markAllAsRead() {
        // store marked entries in a temporary list
        if (isConnected()) {
            final List<String> entryIds = new ArrayList<>(getEntryIds());
            List<String> unreadEntryIds = FeedDatabase.getUnreadEntryIds();
            entryIds.retainAll(unreadEntryIds);

            for (int i = 0; i < entryIds.size(); i++) {
                String entryId = entryIds.get(i);
                // update the network and local database
                FeedDatabase.markEntryAsRead(entryId);
                mFeedFragment.markAsRead(entryId);
            }

            updateLists(false);

            Snackbar.make(getView(), R.string.popup_marked_as_read, 5000)
                    .setAction(R.string.action_undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // mark the previously-read entries as unread from
                            // the temporary list
                            for (int i = 0; i < entryIds.size(); i++) {
                                String entryId = entryIds.get(i);
                                FeedDatabase.markEntryAsUnread(entryId);
                                mFeedFragment.markAsUnread(entryId);
                            }

                            updateLists(false);
                        }
                    })
                    .show();
        }
    }

    /**
     * Marks the currently displayed entries on screen as unread
     * and provides an undo option to reverse the action.
     */
    private void markAllAsUnread() {
        if (isConnected()) {
            // store marked entries in a temporary list
            final List<String> entryIds = new ArrayList<>(getEntryIds());
            List<String> unreadEntryIds = FeedDatabase.getUnreadEntryIds();
            entryIds.removeAll(unreadEntryIds);

            for (int i = 0; i < entryIds.size(); i++) {
                String entryId = entryIds.get(i);
                FeedDatabase.markEntryAsUnread(entryId);
                mFeedFragment.markAsUnread(entryId);
            }

            updateLists(false);

            Snackbar.make(getView(), R.string.popup_marked_as_unread, 5000)
                    .setAction(R.string.action_undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // mark the previously-unread entries as read
                            // from the temporary list
                            for (int i = 0; i < entryIds.size(); i++) {
                                String entryId = entryIds.get(i);
                                FeedDatabase.markEntryAsRead(entryId);
                                mFeedFragment.markAsRead(entryId);
                            }

                            updateLists(false);
                        }
                    })
                    .show();
        }
    }

    /**
     * Refreshes all of the current displays, including the drawer,
     * to display the most recent values.
     */
    private void updateLists(boolean restore) {
        displayUnreadCount();
        displayStarredCount();
        setSelection();

        if (restore) {
            // refresh lists from already-present items
            mRecViewEntries.getAdapter().notifyDataSetChanged();
            mRecViewSubscriptions.getAdapter().notifyDataSetChanged();
            setView();
        } else {
            // get new lists from database
            mFeedFragment.displayTags();
            mFeedFragment.displaySubscriptions();
            displayEntries(false);
        }
    }

    /**
     * Updates the current main view to display the most recent
     * entries of the selected category.
     */
    private void displayEntries(boolean loadMore) {
        int limit;
        String date;

        if (loadMore) {
            // increase the scope of entries to load
            limit = VALUE_INTEGER_ENTRY_SIZE_DEFAULT;
            date = mEntries.get(mEntries.size() - 1).getDate();
            mCurrentEntrySize += VALUE_INTEGER_ENTRY_SIZE_DEFAULT;
        } else {
            // completely refresh the list
            limit = mCurrentEntrySize;
            date = null;
            mFeedFragment.setEntries(new ArrayList<Entry>());
        }

        // fetch entries in background
        mFeedFragment.displayEntries(mSearchQuery, date, limit);
    }

    /**
     * Displays the current number of unread entries
     * next to the unread section of the drawer.
     */
    private void displayUnreadCount() {
        // get the unread count
        String count = "";
        int result = FeedDatabase.getUnreadCount();

        // convert the unread count to a string
        if (result > 0) {
            count = String.valueOf(result);
        }

        // set the unread count in the drawer view
        TextView unreadCount = (TextView)findViewById(R.id.list_drawer_unread_count);
        unreadCount.setText(count);
    }

    /**
     * Displays the current number of starred entries
     * next to the starred section of the drawer.
     */
    private void displayStarredCount() {
        // get the starred count
        String count = "";
        int result = FeedDatabase.getStarredCount();

        // convert the starre count to a string
        if (result > 0) {
            count = String.valueOf(result);
        }

        // set the starred count in the drawer view
        TextView starredCount = (TextView)findViewById(R.id.list_drawer_starred_count);
        starredCount.setText(count);
    }

    private class UpdateReceiver extends BroadcastReceiver {

        private UpdateReceiver() {}

        @Override
        public void onReceive(Context context, Intent intent) {
            SharedPreferencesManager.setValue(SharedPreferencesManager.KEY_BOOLEAN_FIRST_LOGIN, false);
            setRefreshing(false);
            updateLists(false);
        }
    }
}