/* YARR (Yet Another RSS Reader)
 * Copyright (C) 2017-2018  Logan Garcia
 *
 * This file is part of YARR.
 *
 * YARR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * YARR is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YARR.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.cgarcia.yarr;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import me.cgarcia.yarr.object.Outline;
import me.cgarcia.yarr.object.Subscription;

/**
 * Manages processing and namespaces of OPML files.
 *
 * @author  Logan Garcia
 * @version %I%, %G%
 */
public class Opml {
    // strings related to tag elements
    private static final String TAG_OPML = "opml";
    private static final String TAG_HEAD = "head";
    private static final String TAG_TITLE = "title";
    private static final String TAG_BODY = "body";
    private static final String TAG_OUTLINE = "outline";

    // strings related to attribute elements
    private static final String ATTRIBUTE_OPML_VERSION = "version";
    private static final String ATTRIBUTE_OUTLINE_TEXT = "text";
    private static final String ATTRIBUTE_OUTLINE_TITLE = "title";
    private static final String ATTRIBUTE_OUTLINE_TYPE = "type";
    private static final String ATTRIBUTE_OUTLINE_XML_URL = "xmlUrl";

    // strings related to value elements
    private static final String VALUE_XML_ENCODING = "UTF-8";
    private static final String VALUE_OPML_VERSION = "2.0";
    private static final String VALUE_OUTLINE_TYPE = "rss";

    /**
     * Returns a list of OPML outline objects from a valid
     * OPML file.
     *
     * @param in the input stream to read from
     * @return   the list of outlines
     */
    public static List<Outline> read(InputStream in) throws XmlPullParserException, IOException {
        // set the parser
        List<Outline> outlines = new ArrayList<>();
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();
        boolean isOpml = false;

        while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
            // get the current name of the element
            String name = parser.getName();

            // only evaluate of the name is a start tag
            if (name != null && parser.getEventType() == XmlPullParser.START_TAG){
                if (name.equals(TAG_OPML)) {
                    // we are reading a valid OPML document
                    isOpml = true;
                } else if (isOpml && name.equals(TAG_OUTLINE)) {
                    // get the type of the outline element
                    String type = parser.getAttributeValue(null, ATTRIBUTE_OUTLINE_TYPE);

                    // check if we received the expected type
                    if (type != null && type.toLowerCase().equals(VALUE_OUTLINE_TYPE)) {
                        // grab the title if it exists
                        String title = parser.getAttributeValue(null, ATTRIBUTE_OUTLINE_TITLE);

                        if (title == null) {
                            // fallback to grabbing the text if
                            // the title does not exist
                            title = parser.getAttributeValue(null, ATTRIBUTE_OUTLINE_TEXT);
                        }

                        // get the link of the attribute
                        String link = parser.getAttributeValue(null, ATTRIBUTE_OUTLINE_XML_URL);

                        // make sure we have both elements before
                        // adding it to our outlines
                        if (title != null && link != null) {
                            Outline outline = new Outline();
                            outline.setTitle(title);
                            outline.setLink(link);
                            outlines.add(outline);
                        }
                    }
                }
            }

            // continue to parse the document
            parser.next();
        }

        return outlines;
    }

    /**
     * Creates a valid OPML file from a list of subscriptions.
     *
     * @param name          the title of the OPML file
     * @param subscriptions the list of subscriptions
     * @param writer        the output file
     */
    public static void write(String name, List<Subscription> subscriptions, Writer writer) throws IOException {
        // set the serializer with the correct indentation
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(writer);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

        // create the XML and OPML header
        serializer.startDocument(VALUE_XML_ENCODING, false);
        serializer.startTag(null, TAG_OPML);
        serializer.attribute(null, ATTRIBUTE_OPML_VERSION, VALUE_OPML_VERSION);

        // create the head and title elements
        serializer.startTag(null, TAG_HEAD);
        serializer.startTag(null, TAG_TITLE);
        serializer.text(name);
        serializer.endTag(null, TAG_TITLE);
        serializer.endTag(null, TAG_HEAD);

        // create the body element
        serializer.startTag(null, TAG_BODY);

        // loop through each subscription and convert them
        // to an outline element
        for (int i = 0; i < subscriptions.size(); i++) {
            Subscription subscription = subscriptions.get(i);
            serializer.startTag(null, TAG_OUTLINE);
            serializer.attribute(null, ATTRIBUTE_OUTLINE_TEXT, subscription.getName());
            serializer.attribute(null, ATTRIBUTE_OUTLINE_TITLE, subscription.getName());
            serializer.attribute(null, ATTRIBUTE_OUTLINE_TYPE, VALUE_OUTLINE_TYPE);
            serializer.attribute(null, ATTRIBUTE_OUTLINE_XML_URL, subscription.getLink());
            serializer.endTag(null, TAG_OUTLINE);
        }

        // enclose the document
        serializer.endTag(null, TAG_BODY);
        serializer.endTag(null, TAG_OPML);
        serializer.endDocument();
    }
}
